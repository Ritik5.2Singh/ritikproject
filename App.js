import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from './src/Pages/SplashScreen';
import HomePageScreen from './src/Pages/DashBoard';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {Colors} from './src/Colors';
import ExpertConsultation from './src/Pages/ExpertConsultation';
import ExpertDetails from './src/Pages/ExpertDetails';
import PatientDetails from './src/Pages/PatientDetails';
import SignUpPage from './src/Pages/SignUpPage';
import OtpPage from './src/Pages/OtpPage';
import SchedualTime from './src/Pages/SchedualTime';

import {Provider, useSelector} from 'react-redux';
import store from './src/REDUX/Store/store';
import CustomeDrawerNavigation from './src/CustomeDrawerNavigation';
import CustomSidebarMenu from './src/CustomeDrawerNavigation';

import messaging from '@react-native-firebase/messaging';
import {firebase} from '@react-native-firebase/app';
import PushNotification from './src/Examples/PushNotification';
import searched from './src/Examples/Searched';
import Maps from './src/Examples/MAPS/MapDrawer/Maps';

import GoogleMap from './src/Examples/maps1';
import Logout from './src/Pages/Logout';
import DownloadingFile from './src/Examples/DownloadingFile';
import DownloadPdf from './src/Examples/DownloadPdf';
import DownloadingPdf from './src/Examples/downloadingPdf';
import ScheduledAppointments from './src/Pages/ScheduledAppointments';
import MapDrawer from './src/Examples/MAPS/MapDrawer/mapDrawer';
import DoctorCategory from './src/Pages/DoctorCategory';
import ModalDoctordetail from './src/Pages/modalDoctordetail';
import {createContext} from 'react';
import {AppProvider} from './src/AppContextFolder/AppContext';
import AppointmentDownload from './src/Pages/For Downloading Appointment/AppointmentDownload';
import StatusBar from './src/Components/StatusBar';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

function App({navigation}) {
  function ExpertConsultationTitle() {
    return (
      <View style={{flexDirection: 'row'}}>
        <Text style={{color: 'black', fontWeight: '900'}}>
          Expert Consultation
        </Text>
      </View>
    );
  }

  function ScheduledAppointmentsTitle() {
    return (
      <View style={{flexDirection: 'row'}}>
        <Text style={{color: 'black', fontWeight: '900'}}>
          Scheduled Appointments
        </Text>
      </View>
    );
  }

  function PatientDetailTitle() {
    return (
      <View style={{flexDirection: 'row'}}>
        <Text style={{color: 'black', fontWeight: '900'}}>Patient Detail</Text>
      </View>
    );
  }

  function ExpertDetailTitle() {
    return (
      <View style={{flexDirection: 'row'}}>
        <Text style={{color: 'black', fontWeight: '900'}}>Expert Detail</Text>
      </View>
    );
  }

  function DashBoard({navigation}) {
    // Provide the context value with the necessary functions and data

    return (
      <Drawer.Navigator
        initialRouteName="Home"
        drawerContent={props => <CustomeDrawerNavigation {...props} />}>
        <Drawer.Screen
          name="Home"
          component={HomePageScreen}
          options={{
            drawerLabel: 'HOME',
            headerStatusBarHeight: -15,
            headerTitle: StatusBar,
          }}
        />

        <Drawer.Screen
          name="expertConsultation"
          component={ExpertConsultation}
          options={{
            drawerLabel: 'Expert Consultation',
            headerStatusBarHeight: -15,
            headerTitle: ExpertConsultationTitle,
          }}
        />
        {/* <Drawer.Screen name='patientDetail' component={PatientDetails} options={{ headerStatusBarHeight: -15, drawerLabel: 'Patient Details', headerTitle: PatientDetailTitle }} /> */}
        <Drawer.Screen
          name="scheduledAppointments"
          component={ScheduledAppointments}
          options={{
            drawerLabel: 'Scheduled Appointments',
            headerStatusBarHeight: -15,
            headerTitle: ScheduledAppointmentsTitle,
          }}
        />
        <Drawer.Screen
          name="logout"
          component={Logout}
          options={{headerShown: false}}
        />
        <Drawer.Screen
          name="maps"
          component={Maps}
          options={{headerShown: false}}
        />
      </Drawer.Navigator>
    );
  }
  function NavigationPages() {
    // // DATA
    // const user = useSelector((state) => state.user.value.mobileNo);
    // console.log("USER DATA:- " + JSON.stringify(user))
    // // Removed quates
    // let USER = JSON.stringify(user).slice(1, -1);

    // FOR NOTIFICATION

    useEffect(() => {
      messaging()
        .getToken(firebase.app().options.messagingSenderId)
        .then(x => console.log(x))
        .catch(e => console.log(e));
    }, []);

    return (
      <AppProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="dashBoard">
            <Stack.Screen
              name="appointmentDownload"
              component={AppointmentDownload}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="splashScreen"
              component={SplashScreen}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="dashBoard"
              component={DashBoard}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="expertDetails"
              component={ExpertDetails}
              options={{
                headerShown: true,
                headerTitle: ExpertDetailTitle,
                headerStatusBarHeight: -15,
              }}
            />
            <Stack.Screen
              name="signUp"
              component={SignUpPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="otpPage"
              component={OtpPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="schedualTimePage"
              component={SchedualTime}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="home"
              component={HomePageScreen}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="expertConsultation"
              component={ExpertConsultation}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="patientDetail"
              component={PatientDetails}
              options={{headerShown: false}}
            />
            {/* CUSTOM DRAWER */}
            <Stack.Screen
              name="customSidebarMenu"
              component={CustomSidebarMenu}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="scheduledAppointments"
              component={ScheduledAppointments}
              options={{
                headerShown: false,
                header: ({navigation}) => (
                  <CustomeDrawerNavigation navigation={navigation} />
                ),
              }}
            />

            {/* These are for Examples */}
            <Stack.Screen
              name="demoNotification"
              component={PushNotification}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="searched"
              component={searched}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="maps"
              component={Maps}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="googleMap"
              component={GoogleMap}
              options={{headerShown: false}}
            />

            <Stack.Screen
              name="downloadingFile"
              component={DownloadingFile}
              options={{headerShown: false}}
            />
            {/* Referred This in Accord App */}
            <Stack.Screen
              name="downloadPdf"
              component={DownloadPdf}
              options={{headerShown: false}}
            />

            <Stack.Screen
              name="downloadingPdf"
              component={DownloadingPdf}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="mapDrawer"
              component={MapDrawer}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="doctorCategory"
              component={DoctorCategory}
              options={{headerShown: false}}
            />
          </Stack.Navigator>
          <Stack.Screen
            name="modalDoctorDetail"
            component={ModalDoctordetail}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="statusBar"
            component={StatusBar}
            options={{headerShown: false}}
          />
        </NavigationContainer>
      </AppProvider>
    );
  }

  return (
    <Provider store={store}>
      <NavigationPages />
    </Provider>
  );
}

const styles = StyleSheet.create({});

export default App;
