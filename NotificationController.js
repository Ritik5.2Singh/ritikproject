import { Alert, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';


PushNotification.createChannel({
    channelId: 'channel-id',
    channelName: 'My channel',
    channelDescription: 'A chhannel to categorise your notifications',
},
    (created) => console.log(`createChannel returned'${created}'`));


export default function NotificationController(props) {
    useEffect(() => {
        const unsubscribe = messaging().onMessage(async (remoteMessage) => {
            PushNotification.localNotification({
                message: remoteMessage.notification.body,
                title: remoteMessage.notification.title,
                bigPictureUrl: remoteMessage.notification.android.imageUrl,
                smallIcon: remoteMessage.notification.android.imageUrl,
                channelId: true,
                vibrate: true,
            })
        }
        )
        return (
            unsubscribe
        )
    }, []);
    return null
}

const styles = StyleSheet.create({})