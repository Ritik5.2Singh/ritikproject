import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {useState} from 'react';
import {createContext} from 'react';
const DrawerContext = createContext();
export function AppProvider({children}) {
  const [selectedScreen, setSelectedScreen] = useState('Home');

  const context = {
    selectedScreen,
    setSelectedScreen,
  };
  return (
    <DrawerContext.Provider value={context}>{children}</DrawerContext.Provider>
  );
}

const styles = StyleSheet.create({});
export default DrawerContext;
