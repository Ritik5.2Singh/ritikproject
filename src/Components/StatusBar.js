import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Colors} from '../Colors';
import {useNavigation} from '@react-navigation/native';

export default function StatusBar() {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={{flexDirection: 'row'}}
      onPress={() => {
        navigation.navigate('mapDrawer');
      }}>
      <Image
        style={{height: 20, width: 20, tintColor: Colors.common.MAIN2}}
        source={{
          uri: 'https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_location_on_48px-256.png',
        }}
      />
      <Text
        style={{color: Colors.common.MAIN2, fontSize: 15, fontWeight: '400'}}>
        Vadodara
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({});
