// Custom Navigation Drawer / Sidebar with Image and Icon in Menu
import React, {useState} from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  View,
  StyleSheet,
  Image,
  Text,
  Linking,
} from 'react-native';

import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {useSelector} from 'react-redux';
import {Colors} from './Colors';
import {useContext} from 'react';
import {NavigationContext} from '@react-navigation/native';
import DrawerContext from './AppContextFolder/AppContext';

const DRAWER = [
  {
    id: 1,
    title: 'HOME',
    navigateToScreen: 'Home',
    iconUrl:
      'https://cdn0.iconfinder.com/data/icons/google-material-design-3-0/48/ic_home_48px-256.png',
  },
  {
    id: 2,
    title: 'Expert Consultation',
    navigateToScreen: 'expertConsultation',
    iconUrl:
      'https://cdn2.iconfinder.com/data/icons/recruitment-glyph/64/professional-expert-leader-pro-master-512.png',
  },
  {
    id: 3,
    title: 'Scheduled Appointments',
    navigateToScreen: 'scheduledAppointments',
    iconUrl:
      'https://cdn3.iconfinder.com/data/icons/teenyicons-outline-vol-1/15/appointments-256.png',
  },
  // {
  //     id: 4,
  //     title: 'drawermaps',
  //     navigateToScreen: 'maps',
  //     iconUrl: 'https://cdn1.iconfinder.com/data/icons/doctor-5/100/01-1Patient_1-256.png',
  // },
];

const CustomSidebarMenu = ({navigation}) => {
  // For drawer color active
  const [active, setActive] = useState(false);
  //For Drawer Screens change
  const {selectedScreen, setSelectedScreen} = useContext(DrawerContext);
  const handleNavigation = screenName => {
    navigation.navigate(screenName);
    setSelectedScreen(screenName);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {/*Top Large Image */}
      <Image
        source={{
          uri: 'https://cdn3.iconfinder.com/data/icons/font-awesome-solid/512/circle-user-256.png',
        }}
        style={styles.sideMenuProfileIcon}
      />
      <Text style={{textAlign: 'center', color: 'black'}}>Mobile No</Text>
      <DrawerContentScrollView>
        {/* <DrawerItemList {...{props, navigation, state, descriptors}} /> */}
        {DRAWER.map((item, index) => {
          return (
            <TouchableOpacity
              key={index}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor:
                  item.navigateToScreen === selectedScreen
                    ? 'rgba(0,8,50,255)'
                    : '#E9DCDC',
                padding: 10,
                marginHorizontal: 5,
                marginVertical: 8,
                elevation: 8,
                borderRadius: 15,
                borderColor: '#E9DCDC',
                borderWidth: 1,
              }}
              onPress={() => {
                // props.navigation.navigate(item.navigateToScreen),
                //   setActive(index);
                handleNavigation(item.navigateToScreen);
              }}>
              <Image
                style={{
                  height: 24,
                  width: 24,
                  tintColor:
                    selectedScreen === item.navigateToScreen
                      ? 'white'
                      : 'rgba(0,8,50,255)',
                }}
                source={{uri: item.iconUrl}}
              />
              <Text
                style={{
                  color:
                    selectedScreen === item.navigateToScreen
                      ? 'white'
                      : 'rgba(0,8,50,255)',
                  fontWeight: '500',
                  paddingLeft: 10,
                }}>
                {item.title}
              </Text>
            </TouchableOpacity>
          );
        })}
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            backgroundColor: Colors.common.MAIN1,
            padding: 10,
            marginHorizontal: 5,
            marginVertical: 8,
            elevation: 8,
            borderRadius: 15,
            borderColor: '#E9DCDC',
            borderWidth: 1,
          }}
          onPress={() => props.navigation.navigate('logout')}>
          <Text style={{color: 'white', fontWeight: '500', paddingLeft: 10}}>
            Logout
          </Text>
          <Image
            style={{height: 24, width: 24, tintColor: 'white'}}
            source={{
              uri: 'https://cdn4.iconfinder.com/data/icons/tabler-vol-4/24/logout-256.png',
            }}
          />
        </TouchableOpacity>
      </DrawerContentScrollView>
      <Text style={{fontSize: 16, textAlign: 'center', color: 'grey'}}>
        www.accord.com
      </Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sideMenuProfileIcon: {
    resizeMode: 'center',
    width: 50,
    height: 50,
    borderRadius: 100 / 2,
    alignSelf: 'center',
    tintColor: '#3549',
  },
  iconStyle: {
    width: 15,
    height: 15,
    marginHorizontal: 5,
  },
  customItem: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default CustomSidebarMenu;
