import { configureStore } from "@reduxjs/toolkit";
import UserDetailReducer from "../reducers/test_reducer";

const store = configureStore({
    reducer: {
        user: UserDetailReducer,
    }
})
export default store;