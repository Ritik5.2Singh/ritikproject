import { combineReducers } from "redux";
import UserDetailReducer from "./test_reducer";

export const reducers = combineReducers({
    UserDetails: UserDetailReducer,
})