
import { createSlice } from "@reduxjs/toolkit";
export const UserDetailReducer = createSlice({

    name: 'user',
    initialState: {
        value: null,
        doctData: [],
        searchedExpert: [],
        scheduleAppointment: [],

    },
    reducers: {
        login: (state, action) => {
            state.value = action.payload;
        },
        DoctorInfo: (state, action) => {//for getting Expert Data 
            state.doctData = action.payload;
        },
        searchDocDashboard: (state, action) => { //for doctor search in Dashboard
            state.searchedExpert = action.payload;
        },
        ScheduleAppointment: (state, action) => {//For Scheduling Appointment's data
            state.scheduleAppointment = action.payload;
        },

        logout: (state) => {
            state.value = null
        }
    }
})
export const { login, logout, DoctorInfo, searchDocDashboard, ScheduleAppointment, } = UserDetailReducer.actions;

// export const selectUser = (state) => state.mobileNo;
export default UserDetailReducer.reducer