import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Modal,
  Button,
} from 'react-native';
import React from 'react';
import ModalDoctordetail from './modalDoctordetail';
import {useState} from 'react';
const DATA = [
  {
    id: 1,
    imgSrc: require('../Assets/Images/DoctorsPic/one.webp'),
    name: 'Dr.Prashant Patel',
    work: 'Pediatrics',
    time: '10 years',
    rating: '4.4',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 196,
    onlineConsultion: 500,
    visitClinic: 700,
  },
  {
    id: 2,
    imgSrc: require('../Assets/Images/DoctorsPic/two.webp'),
    name: 'Dr.Mashoor Gulati',
    work: 'Pediatrics',
    time: '14 years',
    rating: '4.8',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 276,
    onlineConsultion: 700,
    visitClinic: 300,
  },
  {
    id: 3,
    imgSrc: require('../Assets/Images/DoctorsPic/three.webp'),
    name: 'Dr.Anup Singh',
    work: 'Naturopathic',
    time: '13 years',
    rating: '4.5',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 201,
    onlineConsultion: 700,
    visitClinic: 900,
  },
  {
    id: 4,
    imgSrc: require('../Assets/Images/DoctorsPic/four.webp'),
    name: 'Dr.Arohi Shah',
    work: 'Pediatrics',
    time: '19 years',
    rating: '4.5',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 205,
    onlineConsultion: 450,
    visitClinic: 890,
  },
  {
    id: 5,
    imgSrc: require('../Assets/Images/DoctorsPic/five.webp'),
    name: 'Dr.Vikash Joshi',
    work: 'Pediatrics',
    time: '8 years',
    rating: '4.9',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 360,
    onlineConsultion: 580,
    visitClinic: 770,
  },
  {
    id: 6,
    imgSrc: require('../Assets/Images/DoctorsPic/six.webp'),
    name: 'Dr.Vikram Joshi',
    work: 'Pediatrics',
    time: '15 years',
    rating: '4.8',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 220,
    onlineConsultion: 210,
    visitClinic: 100,
  },
  {
    id: 7,
    imgSrc: require('../Assets/Images/DoctorsPic/seven.webp'),
    name: 'Dr.Vinod Negi',
    work: 'Naturopathic',
    time: '12 years',
    rating: '4.5',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 303,
    onlineConsultion: 1000,
    visitClinic: 900,
  },
  {
    id: 8,
    imgSrc: require('../Assets/Images/DoctorsPic/eight.webp'),
    name: 'Dr.Prabhat Rawat',
    work: 'Pediatrics',
    time: '9 years',
    rating: '4.0',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 202,
    onlineConsultion: 900,
    visitClinic: 800,
  },
  {
    id: 9,
    imgSrc: require('../Assets/Images/DoctorsPic/nine.webp'),
    name: 'Dr.Hemant Joshi',
    work: 'Pediatrics',
    time: '5 years',
    rating: '4.7',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 180,
    onlineConsultion: 1100,
    visitClinic: 790,
  },
  {
    id: 10,
    imgSrc: require('../Assets/Images/DoctorsPic/ten.webp'),
    name: 'Dr.Priyadeep Chaudhari',
    work: 'Naturopathic',
    time: '11 years',
    rating: '4.1',
    bookAppointment: 'expertDetails',
    OnlineConsultantCompleted: 280,
    onlineConsultion: 680,
    visitClinic: 940,
  },
];
export default function DoctorCategory({navigation}) {
  //
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={{backgroundColor: '#F6F2F2'}}>
      {/* HEADER */}
      <View style={styles.header}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={{
              uri: 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-arrow-left-512.png',
            }}
            style={styles.headerIcon}
          />
        </TouchableOpacity>
        {/* For search input */}
        <View style={styles.searchInput}>
          <TextInput placeholder="Search" />

          <Image
            source={{
              uri: 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-search-strong-512.png',
            }}
            style={styles.headerIcon}
          />
        </View>
        {/* Profile picture */}
        <Image
          style={styles.profileIcon}
          source={{
            uri: 'https://imgs.search.brave.com/BSSXJzr9R6ZXUkD5rpnCIU7KCxC4NgN4QhJc3M7gxHc/rs:fit:960:1024:1/g:ce/aHR0cHM6Ly9iZXN0/cHJvZmlsZXBpY3R1/cmVzLmNvbS93cC1j/b250ZW50L3VwbG9h/ZHMvMjAyMS8wNC9D/b29sLVByb2ZpbGUt/UGljdHVyZS1Gb3It/WW91dHViZS05NjB4/MTAyNC5qcGc',
          }}
        />
      </View>
      <View style={{borderBottomWidth: 1, borderColor: 'black'}} />
      {/* Doctor's DATA */}
      <ScrollView>
        {
          <View>
            {DATA.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    setModalVisible(true);
                  }}
                  style={styles.doctor}>
                  <Image
                    resizeMode="stretch"
                    style={styles.doctorImg}
                    source={item.imgSrc}
                  />
                  <View style={{flex: 1, marginHorizontal: 15}}>
                    <View>
                      <Text style={{fontWeight: 'bold', color: 'black'}}>
                        {item.name}
                      </Text>
                      <Text>{item.work}</Text>
                    </View>
                  </View>
                  <Image
                    style={{height: 20, width: 20}}
                    source={{
                      uri: 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-arrow-right-512.png',
                    }}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
        }
      </ScrollView>
      {/* Dialog box */}
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Modal
          transparent={true}
          visible={modalVisible}
          animationType="slide"
          onRequestClose={() => setModalVisible(false)}
          style={{height: 200, width: '80%', alignSelf: 'center'}}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'white',
            }}>
            <Text>This is a modal dialog!</Text>
            <Button title="Close" onPress={() => setModalVisible(false)} />
          </View>
        </Modal>
        <Button
          title="Other Button"
          onPress={() => console.log('Other button clicked')}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  headerIcon: {
    width: 20,
    height: 20,
  },
  profileIcon: {
    width: 40,
    height: 40,
    borderRadius: 5,
  },
  searchInput: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
    paddingHorizontal: 20,
    flex: 1,
    justifyContent: 'space-between',
  },
  doctor: {
    flexDirection: 'row',
    backgroundColor: 'white',
    margin: 10,
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
  },
  doctorImg: {height: 50, width: 50, borderRadius: 10},
});
