import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useRef} from 'react';
import {Colors} from '../Colors';
import {useDispatch, useSelector} from 'react-redux';
import {ScheduleAppointment} from '../REDUX/reducers/test_reducer';
import Toast from 'react-native-easy-toast';
import {useEffect} from 'react';

export default function SchedualTime({navigation}) {
  const appointmentPatientDoctorDetails = useSelector(
    state => state.user.searchedExpert.searchedExpert,
  );

  console.log(
    'Appointment going to Booked :-' +
      JSON.stringify(appointmentPatientDoctorDetails, null, 1),
  );

  const Data1 = [
    {
      key: 0,
      date: '10 Jan',
      seats: 11,
    },
    {
      key: 1,
      date: '11 Jan',
      seats: 'No',
    },
    {
      key: 2,
      date: '12 Jan',
      seats: 11,
    },
    {
      key: 3,
      date: '13 Jan',
      seats: 20,
    },
    {
      key: 4,
      date: '14 Jan',
      seats: 5,
    },
    {
      key: 5,
      date: '15 Jan',
      seats: 19,
    },
  ];

  const Data2 = [
    {
      key: 0,
      shifts: 'Morning',
      src: 'https://cdn0.iconfinder.com/data/icons/30px-weather/30/07_weather-sun-morning-sunrise-forecast-512.png',
      timing: 'No Slots available',
      timings: [
        {
          id: 0,
          time: 'No Slots available',
        },
      ],
    },
    {
      key: 1,
      shifts: 'Afternoon',
      src: 'https://cdn0.iconfinder.com/data/icons/30px-weather/30/07_weather-sun-morning-sunrise-forecast-512.png',
      timing: '11:00 am to 1:00 am',
      timings: [
        {
          id: 0,
          time: '11:00 pm',
        },
        {
          id: 1,
          time: '11:20 pm',
        },
        {
          id: 2,
          time: '11:40 pm',
        },
        {
          id: 3,
          time: '12:00 pm',
        },
        {
          id: 4,
          time: '12:20 pm',
        },
        {
          id: 5,
          time: '12:40 pm',
        },
        {
          id: 6,
          time: '1:00 pm',
        },
        // {
        //     id: 7,
        //     time: '1:20 pm',
        // },
        // {
        //     id: 8,
        //     time: '1:40 pm',
        // },
        // {
        //     id: 9,
        //     time: '2:00 pm',
        // },
      ],
    },
    {
      key: 2,
      shifts: 'Evening',
      src: 'https://cdn0.iconfinder.com/data/icons/30px-weather/30/07_weather-sun-morning-sunrise-forecast-512.png',
      timing: '1:00 am to 3:00 am',
      timings: [
        {
          id: 0,
          time: '1:00 pm',
        },
        {
          id: 1,
          time: '1:20 pm',
        },
        {
          id: 2,
          time: '1:40 pm',
        },
        {
          id: 3,
          time: '2:00 pm',
        },
        {
          id: 4,
          time: '2:20 pm',
        },
        {
          id: 5,
          time: '2:40 pm',
        },
        {
          id: 6,
          time: '3:00 pm',
        },
      ],
    },
  ];

  const [hideSession, showSession] = useState(false);
  const [select, selectTime] = useState(false);
  // for SchedualTime page
  // const [list, setList] = useState([]);
  //For Appoinment
  const [time, setTime] = useState({date: '10 Jan', timing: 'No Timing'});

  const SelectingTime = (timeItem, timeIndex) => {
    setTime(time => ({...time, timing: timeItem.time}));
    selectTime(timeIndex);

    const alphabets = timeItem.time.match(/[a-zA-Z]/g);
    const numbers = timeItem.time.match(/[0-9]/g);

    if (alphabets.length > numbers.length) {
      console.log('String contains more alphabetic characters');
    } else if (numbers.length > alphabets.length) {
      console.log('String contains more numbers');
    } else {
      console.log(
        'String contains equal number of alphabetic characters and numbers',
      );
    }
  };
  // FOR CURRENT DATE
  var today = new Date();
  let hours = today.getHours();
  let minutes = today.getMinutes();
  const ampm = hours >= 12 ? ' PM' : ' AM';
  var currentTime = hours + ':' + minutes + ampm;
  const daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  var date = today.toLocaleDateString();
  var day = daysOfWeek[today.getDay()];

  // var time = today.getHours() + ":" + today.getMinutes()+ today.;

  const newObj = {
    timeWhenSchedule: currentTime,
    dateWhenSchedule: date,
    dayWhenSchedule: day,
  };

  // Booked Appointment
  const dispatchBooking = useDispatch();
  //Toast Message
  const ToastRef = useRef(null);

  const AppointmentBookBtn = () => {
    let BookingPatient = Object.assign(
      {},
      appointmentPatientDoctorDetails,
      newObj,
      time,
    );
    dispatchBooking(ScheduleAppointment({scheduleAppointment: BookingPatient}));

    console.log('Date Selected:-', time.date, 'Timing Seleted:-', time.timing);

    //Toast Message
    ToastRef.current.show('Appointment Booked Successsfully');
    console.log(
      'AppointmentPatientDoctorDetails :=',
      appointmentPatientDoctorDetails,
    );
    // for lists in SchedualTime page
    // setList([patient, ...list]);
  };

  return (
    <ScrollView style={{backgroundColor: 'white'}}>
      <View style={{marginHorizontal: 10}}>
        <View style={{flexDirection: 'row', marginVertical: 10}}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              style={{height: 20, width: 20}}
              source={{
                uri: 'https://cdn1.iconfinder.com/data/icons/essentials-pack/96/left_arrow_back_previous_navigation-256.png',
              }}
            />
          </TouchableOpacity>
          <Text style={{fontWeight: '700', color: 'black'}}>
            Visit Clinic (Physically)
          </Text>
        </View>
        <Text style={{fontWeight: '800', fontSize: 18, color: 'black'}}>
          Schedual
        </Text>
      </View>
      {/* First scrolls */}

      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        showSessionsHorizontalScrollIndicator={false}>
        {Data1.map((item, index) => {
          return (
            <TouchableOpacity
              onPress={() => {
                setTime({date: item.date});
              }}
              key={index}
              style={{
                backgroundColor: 'white',
                height: 55,
                marginRight: 3,
                width: 130,
                alignItems: 'center',
                paddingVertical: 10,
                marginVertical: 10,
                borderWidth: 1,
                borderColor: 'rgba(252,93,163,255)',
                borderStyle: 'dashed',
              }}>
              <Text style={{color: 'black'}}>{item.date}</Text>
              <Text style={{color: Colors.common.MAIN2, fontSize: 10}}>
                {item.seats} Slots available
              </Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>

      <Text
        style={{
          marginVertical: 15,
          marginHorizontal: 12,
          color: Colors.common.MAIN2,
        }}>
        {time.date}
      </Text>

      {/* For Card */}
      {Data2.map((item, index) => {
        return (
          <>
            <View key={index} style={{marginHorizontal: 10, marginVertical: 5}}>
              <TouchableOpacity
                onPress={() => {
                  showSession(index);
                }}
                style={{
                  flexDirection: 'row',
                  backgroundColor:
                    hideSession == index ? 'rgba(0,8,50,255)' : 'white',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    flexGrow: 1,
                    alignItems: 'center',
                    padding: 5,
                  }}>
                  <Image
                    style={{
                      height: 25,
                      width: 25,
                      tintColor: Colors.common.MAIN2,
                    }}
                    source={{uri: item.src}}
                  />
                  <Text
                    style={{
                      color: hideSession == index ? 'white' : 'black',
                      fontWeight: '600',
                      marginHorizontal: 10,
                    }}>
                    {item.shifts}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    flexGrow: 2,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 9,
                      color: hideSession == index ? 'white' : null,
                    }}>
                    {item.timing}
                  </Text>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      tintColor:
                        hideSession == index
                          ? 'white'
                          : 'rgba(152,152,153,255)',
                      justifyContent: 'flex-end',
                    }}
                    source={{
                      uri:
                        hideSession == index
                          ? 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-arrow-down-256.png'
                          : 'https://cdn4.iconfinder.com/data/icons/navigation-40/24/chevron-right-512.png',
                    }}
                  />
                </View>
              </TouchableOpacity>

              {/* Timings */}
              {hideSession == index && (
                <View style={{alignItems: 'center'}}>
                  <View
                    style={{
                      backgroundColor: 'white',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'flex-start',
                    }}>
                    {item.timings.map((timeItem, timeIndex) => {
                      return (
                        <TouchableOpacity
                          onPress={() => {
                            SelectingTime(timeItem, timeIndex);
                          }}
                          key={timeItem.id}
                          style={{
                            marginVertical: 10,
                            marginHorizontal: 10,
                            backgroundColor:
                              select == timeIndex
                                ? 'rgba(0,8,50,255)'
                                : 'white',
                            borderWidth: 1,
                            borderColor: 'rgba(231,230,231,255)',
                          }}>
                          <Text
                            style={{
                              margin: 6,
                              color: select == timeIndex ? 'white' : 'black',
                            }}>
                            {timeItem.time}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                </View>
              )}
            </View>
          </>
        );
      })}
      <TouchableOpacity
        onPress={() => {
          AppointmentBookBtn();
        }}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          alignSelf: 'center',
          marginVertical: 30,
        }}>
        <Text style={{color: 'rgba(246,15,70,252)'}}>Continue </Text>
        <Image
          style={{height: 13, width: 13, tintColor: 'rgba(246,15,70,252)'}}
          source={{
            uri: 'https://cdn1.iconfinder.com/data/icons/lightly-selected/30/forward-alt-240.png',
          }}
        />
      </TouchableOpacity>
      <Toast ref={ToastRef} position="center" backgroundColor="blue" />
    </ScrollView>
  );
}

const styles = StyleSheet.create({});
