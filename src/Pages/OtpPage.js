import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Dimensions } from 'react-native'
import React from 'react'
import { Colors } from '../Colors'
import LinearGradient from 'react-native-linear-gradient'

export default function OtpPage({ navigation }) {
    return (
        <>
            <View style={{ marginHorizontal: 20, marginTop: 80 }}>
                {/* Back Button */}
                <TouchableOpacity onPress={() => navigation.goBack()} style={{ alignSelf: 'flex-start' }}>
                    <Image style={{ height: 20, width: 20, marginVertical: 15 }} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/essentials-pack/96/left_arrow_back_previous_navigation-512.png' }} />
                </TouchableOpacity>
                <Text style={{ color: 'black', fontSize: 20, fontWeight: '700' }}>Enter Your 6 Digit <Text style={{ color: Colors.common.MAIN2 }}>OTP</Text></Text>

                {/*OTP Input */}
                <View style={{ flexDirection: 'row', marginVertical: 20, paddingVertical: 8, alignSelf: 'center', justifyContent: 'space-around' }}>

                    <View style={{
                        marginHorizontal: 10,
                        backgroundColor: '#fff',
                        elevation: 3,
                        height: 60,
                        width: 45,
                        borderRadius: 5,
                        justifyContent: 'center',
                        borderRightWidth: 3,
                        borderRightColor: 'rgba(195,195,199,255)',
                        borderBottomWidth: 3,
                        borderBottomColor: 'rgba(195,195,199,255)',
                    }}>
                        <TextInput style={{ alignSelf: 'center', fontSize: 25 }} maxLength={1} />
                    </View>
                    <View style={{
                        marginHorizontal: 10,
                        backgroundColor: '#fff',
                        elevation: 3,
                        height: 60,
                        width: 45,
                        borderRadius: 5,
                        justifyContent: 'center',
                        borderRightWidth: 3,
                        borderRightColor: 'rgba(195,195,199,255)',
                        borderBottomWidth: 3,
                        borderBottomColor: 'rgba(195,195,199,255)',
                    }}>
                        <TextInput style={{ alignSelf: 'center', fontSize: 25 }} maxLength={1} />
                    </View>
                    <View style={{
                        marginHorizontal: 10,
                        backgroundColor: '#fff',
                        elevation: 3,
                        height: 60,
                        width: 45,
                        borderRadius: 5,
                        justifyContent: 'center',
                        borderRightWidth: 3,
                        borderRightColor: 'rgba(195,195,199,255)',
                        borderBottomWidth: 3,
                        borderBottomColor: 'rgba(195,195,199,255)',
                    }}>
                        <TextInput style={{ alignSelf: 'center', fontSize: 25 }} maxLength={1} />
                    </View>
                    <View style={{
                        marginHorizontal: 10,
                        backgroundColor: '#fff',
                        elevation: 3,
                        height: 60,
                        width: 45,
                        borderRadius: 5,
                        justifyContent: 'center',
                        borderRightWidth: 3,
                        borderRightColor: 'rgba(195,195,199,255)',
                        borderBottomWidth: 3,
                        borderBottomColor: 'rgba(195,195,199,255)',
                    }}>
                        <TextInput style={{ alignSelf: 'center', fontSize: 25 }} maxLength={1} />
                    </View>
                    <View style={{
                        marginHorizontal: 10,
                        backgroundColor: '#fff',
                        elevation: 3,
                        height: 60,
                        width: 45,
                        borderRadius: 5,
                        justifyContent: 'center',
                        borderRightWidth: 3,
                        borderRightColor: 'rgba(195,195,199,255)',
                        borderBottomWidth: 3,
                        borderBottomColor: 'rgba(195,195,199,255)',
                    }}>
                        <TextInput style={{ alignSelf: 'center', fontSize: 25 }} maxLength={1} />
                    </View>

                    <View style={{
                        marginHorizontal: 10,
                        backgroundColor: '#fff',
                        elevation: 3,
                        height: 60,
                        width: 45,
                        borderRadius: 5,
                        justifyContent: 'center',
                        borderRightWidth: 3,
                        borderRightColor: 'rgba(195,195,199,255)',
                        borderBottomWidth: 3,
                        borderBottomColor: 'rgba(195,195,199,255)',
                    }}>
                        <TextInput style={{ alignSelf: 'center', fontSize: 25 }} maxLength={1} />
                    </View>

                </View>

                {/*Button For Resend OTP */}
                <Text style={{ fontSize: 12, marginVertical: 5, color: 'black-' }}>Don't receive an OTP?</Text>

                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 20 }}>
                    <Text style={{ color: Colors.common.MAIN2, fontSize: 12 }}>Resend OTP </Text>

                </TouchableOpacity>
                {/* Button for Submitting mobile no */}
                <TouchableOpacity onPress={() => { navigation.navigate('dashBoard') }} style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center', }}>
                    <Text style={{ color: Colors.common.MAIN2, fontSize: 16 }}>Submit </Text>
                    <Image style={{ height: 17, width: 17, tintColor: Colors.common.MAIN2, }} source={{ uri: 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-right-c-512.png' }} />
                </TouchableOpacity>
            </View>
        </>
    )
}

const styles = StyleSheet.create({})