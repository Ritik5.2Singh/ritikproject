import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  Modal,
  FlatList,
  SafeAreaView,
  TouchableHighlight,
} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

import {useState} from 'react';
import {useEffect} from 'react';
import {useContext} from 'react';

import DrawerContext from '../AppContextFolder/AppContext';

import RNHTMLtoPDF from 'react-native-html-to-pdf';
export default function ScheduledAppointments({navigation}) {
  const patient = useSelector(
    state => state.user.scheduleAppointment.scheduleAppointment,
  );
  console.log('PATIENT:-', JSON.stringify(patient, null, 2));
  const [patientArray, setPatientArray] = useState([]);
  // For Array Listing
  useEffect(() => {
    if (patient != undefined) {
      // patientArray.splice(0, 0, patient);
      patientArray.unshift(patient);
    } else {
      console.log('UNDEFINED Patient');
    }
  }, [patient]);

  console.log('Patient Array:-', JSON.stringify(patientArray, null, 2));
  console.log('Length of Patient Array:-', patientArray.length);
  const [isModalVisible, setIsModalVisible] = useState(false);
  // For Drawer Navigation
  const {setSelectedScreen} = useContext(DrawerContext);
  const goToBack = () => {
    try {
      setSelectedScreen('Home');
      navigation.navigate('Home');
    } catch (error) {
      console.log('Error On going:-', error);
    }
  };
  //For Pop Ups of Menu
  const [selectMenu, setSelectMenu] = useState([]);
  const handleShowPopups = item => {
    // setIsModalVisible(!isModalVisible);
    if (selectMenu.includes(item)) {
      setSelectMenu(selectMenu.filter(select => select != item));
    } else {
      setSelectMenu([...selectMenu, item]);
    }
  };
  //For Downloading PDF
  const [filePath, setFilePath] = useState(''); //For Downloading Appointment PDF
  const DownloadPdf = async item => {
    let options = {
      html: `<body style="text-align: center;>

      <img src="https://cdn2.iconfinder.com/data/icons/plump-by-zerode_/256/Folder-Scheduled-Tasks-icon.png" alt="IMAGE" style="width:48px; height:48px;">
        <br>
        <h1>
      <strong>
          Appointment
          </strong>
      </h1>
        <div>
        <img src="https://cdn2.iconfinder.com/data/icons/plump-by-zerode_/256/Document-scheduled-tasks-icon.png" alt="IMAGE" style="width:48px; height:48px;">
        </div>
        <p >Rahul, we've got you confirmed for your appointment.</p>
        <h3>${item.timeWhenSchedule} | ${item.name} </h3>
        <p>
        <Strong>TIMING:- ${item.timing}</Strong>
        <br>
<i>${item.dateWhenSchedule}</i>
<br>
        
        <br>
        <Strong>Address</Strong>
        </p>
  </body>
  `,
      filename: 'appointmentPDF',
      directory: 'docs',
    };
    let file = await RNHTMLtoPDF.convert(options);
    console.log('File path where Downloaded:', file.filePath);
    setFilePath(file.filePath);
  };
  return (
    <SafeAreaView>
      {patientArray.length === 0 ? (
        <ScrollView
          contentContainerStyle={{
            alignItems: 'center',
            height: '90%',
            justifyContent: 'center',
          }}>
          <Image
            style={{height: 100, width: 100, tintColor: '#C7C7C7'}}
            source={{
              uri: 'https://cdn3.iconfinder.com/data/icons/teenyicons-outline-vol-1/15/appointments-256.png',
            }}
          />
          <Button
            title="Back"
            onPress={() => {
              console.log('back to Home page.');
              goToBack();
            }}
          />
        </ScrollView>
      ) : (
        <ScrollView>
          {patientArray.map((item, index) => {
            return (
              <View key={index} style={{flex: 1}}>
                <TouchableOpacity
                  onPress={() => {
                    // For Hiding menu
                    // setSelectMenu(selectMenu.filter(select => select != item));
                  }}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    elevation: 1,
                    margin: 5,
                    borderBottomColor: 'grey',
                    flexDirection: 'row',
                    alignSelf: 'center',
                    backgroundColor: 'white',
                    padding: 10,
                    elevation: 5,
                    borderColor: '#E9DCDC',
                  }}>
                  <Image
                    style={{height: 30, width: 30}}
                    source={{
                      uri: 'https://cdn0.iconfinder.com/data/icons/medical-2372/32/Patient-256.png',
                    }}
                  />

                  <View
                    style={{
                      height: 50,
                      width: Dimensions.get('screen').width / 1.5,
                      marginHorizontal: 12,
                    }}>
                    <Text
                      style={{color: 'rgba(0,8,50,255)', fontWeight: 'bold'}}>
                      {item.name}
                    </Text>
                    <Text style={{color: 'rgb(30,129,176)'}}>
                      {item.dayWhenSchedule} {item.dateWhenSchedule}
                    </Text>
                  </View>
                  <Text style={{color: 'rgba(0,8,50,255)'}}>
                    {item.timeWhenSchedule}
                  </Text>
                  {/* For Menu of Each */}
                  {/* <View> */}

                  {/* </View> */}
                  <TouchableOpacity
                    onPress={() => {
                      handleShowPopups(item);
                    }}>
                    <Image
                      source={{
                        uri: 'https://cdn0.iconfinder.com/data/icons/zondicons/20/dots-horizontal-triple-512.png',
                      }}
                      style={{height: 20, width: 20}}
                    />
                  </TouchableOpacity>
                  {selectMenu.includes(item) ? (
                    <View
                      style={{
                        // justifyContent: 'center',
                        // alignItems: 'center',
                        padding: 5,
                        position: 'absolute',
                        right: 0,
                        top: 40,
                        marginHorizontal: 24,
                        elevation: 1,
                        shadowOpacity: 0.25,
                        shadowOffset: {width: 4, height: 2},
                        shadowRadius: 6,
                        backgroundColor: '#F6F6F6',
                        zIndex: 9,
                        borderColor: 'grey',
                      }}>
                      {/* <View style={{}}> */}
                      <TouchableOpacity>
                        <Text style={{fontSize: 12}}>View</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
                          DownloadPdf(item);
                        }}>
                        <Text style={{fontSize: 12}}>Download</Text>
                      </TouchableOpacity>
                      {/* </View> */}
                    </View>
                  ) : null}
                </TouchableOpacity>
              </View>
            );
          })}
        </ScrollView>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({});
{
  /* <FlatList
            data={patientData}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <View>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    elevation: 1,
                    margin: 5,
                    borderBottomColor: 'grey',
                    flexDirection: 'row',
                    alignSelf: 'center',
                    backgroundColor: 'white',
                    padding: 10,
                    elevation: 5,
                    borderColor: '#E9DCDC',
                  }}>
                  <Image
                    style={{height: 30, width: 30}}
                    source={{
                      uri: 'https://cdn0.iconfinder.com/data/icons/medical-2372/32/Patient-256.png',
                    }}
                  />
                 
                  <View
                    style={{
                      height: 50,
                      width: Dimensions.get('screen').width / 1.5,
                      marginHorizontal: 12,
                    }}>
                    <Text
                      style={{color: 'rgba(0,8,50,255)', fontWeight: 'bold'}}>
                      {item.name}
                    </Text>
                    <Text style={{color: 'rgb(30,129,176)'}}>
                      {item.dayWhenSchedule} {item.dateWhenSchedule}
                    </Text>
                  </View>
                  <Text style={{color: 'rgba(0,8,50,255)'}}>
                    {item.timeWhenSchedule}
                  </Text>
                  
                  <TouchableOpacity
                    onPress={() => {
                      handleShowPopups(index);
                    }}>
                    <Image
                      source={{
                        uri: 'https://cdn0.iconfinder.com/data/icons/zondicons/20/dots-horizontal-triple-512.png',
                      }}
                      style={{height: 20, width: 20}}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                {selectedModal == item.id ? (
                  <View>
                    <Text>View</Text>
                  </View>
                ) : null}
              </View>
            )}
          /> */
}
