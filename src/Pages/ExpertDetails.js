import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, ScrollView, CheckBox } from 'react-native'
import React, { useState } from 'react'
import { Colors } from '../Colors'
import { useDispatch, useSelector } from 'react-redux'
import { ScheduleAppointment } from '../REDUX/reducers/test_reducer'

export default function ExpertDetails({ navigation }) {
    // for showing Data after clicking book appointment 
    const [show, hide] = useState(false)
    //Getting Expert Details
    const expertDetails = useSelector((state) => state.user.searchedExpert.searchedExpert)
    console.log('Expert Details =>', JSON.stringify(expertDetails, null, 1).length)
    return (
        <ScrollView style={{ backgroundColor: Colors.light.BackG, height: '100%', }}>
            {/* Doctor's Photo */}
            <Image style={{ alignSelf: 'center', height: 55, width: 55, borderRadius: 50, marginTop: 20 }} source={expertDetails.imgSrc} />
            {/* Doctor's Name */}
            <Text style={{ color: Colors.dark.TEXT, textAlign: 'center' }}>{expertDetails.name}</Text>
            {/* Doctor's Ocupation */}
            <Text style={{ textAlign: 'center', fontSize: 8, fontWeight: '400', color: Colors.common.MAIN2, marginBottom: 20 }}>{expertDetails.work}</Text>


            <View style={{ backgroundColor: Colors.dark.TEXT, borderTopStartRadius: 30, borderTopRightRadius: 30, }}>

                {/* <View style={{ height: -5 }}> */}
                <View style={{ flexDirection: 'row', alignSelf: 'center', bottom: 12 }}>
                    <TouchableOpacity style={{ borderRadius: 50, backgroundColor: Colors.common.MAIN2, marginHorizontal: 5 }}>
                        <Image style={{
                            height: 15, width: 15, margin: 5, tintColor:
                                'white'
                        }} source={{ uri: 'https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/video_camera-512.png' }} />
                    </TouchableOpacity>

                    <TouchableOpacity style={{ borderRadius: 50, backgroundColor: Colors.common.MAIN2, marginHorizontal: 5 }}>
                        <Image style={{
                            height: 15, width: 15, margin: 5, tintColor:
                                'white'
                        }} source={{ uri: 'https://cdn0.iconfinder.com/data/icons/school-education-60/70/Chat_communication_conversation_messages-512.png' }} />
                    </TouchableOpacity>
                </View>
                {/* </View> */}

                {/*Doctor's Details */}
                <View style={{ height: Dimensions.get('screen').height / 1 }}>

                    <View style={{ marginHorizontal: 18, marginVertical: 25, }}>
                        <Text style={{ color: 'black', fontWeight: '600', marginVertical: 5 }}>About<Text style={{ color: Colors.common.MAIN2, fontWeight: 'bold', }}> Doctor</Text></Text>

                        <Text style={{ color: Colors.common.MAIN2, fontSize: 10, marginVertical: 5, fontWeight: 'bold', }}>Experience</Text>
                        <Text style={styles.text}>{expertDetails.time} Experience</Text>
                        <Text style={styles.text}>{expertDetails.OnlineConsultantCompleted} Online Consultaions Completed</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 5 }}>
                            <Image style={{ height: 10, width: 10, tintColor: Colors.common.MAIN2 }} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/education-1-42/48/26-512.png' }} />
                            <Text style={{ color: 'black', fontSize: 10, fontWeight: '700' }} > MBBS</Text>
                        </View>

                        <Text style={{ color: Colors.common.MAIN2, fontSize: 10, fontWeight: 'bold', marginVertical: 5 }}>Next Available</Text>

                        {/* Timing of Doctor  */}
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexGrow: 1, marginHorizontal: 4 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image style={{ height: 13, width: 13, tintColor: 'rgba(1,33,92,255)' }} source={{ uri: 'https://cdn3.iconfinder.com/data/icons/linecons-free-vector-icons-pack/32/video-512.png' }} />
                                    <Text style={{ color: 'rgba(1,33,92,255)', fontSize: 9, fontWeight: '700' }}>  9:00 am, Tomorrow</Text>
                                </View>

                                <View style={{ borderTopWidth: 1, borderRightWidth: 1, marginVertical: 5, padding: 4, borderStyle: 'dashed', borderColor: Colors.common.MAIN2, backgroundColor: 'rgba(243,242,243,255)' }}>
                                    <Text style={{ color: Colors.common.MAIN2, }}>Rs {expertDetails.onlineConsultion}</Text>
                                    <Text style={{ fontSize: 9, color: 'black', fontWeight: '700' }}>Online Consultaion</Text>
                                </View>
                            </View>
                            <View style={{ flexGrow: 1, marginHorizontal: 4, }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                                    <Image style={{ height: 13, width: 13, tintColor: 'rgba(1,33,92,255)' }} source={{ uri: 'https://cdn4.iconfinder.com/data/icons/hygiene-and-cleaning-1/64/clinic-hospital-building-drug-store-medical-512.png' }} />
                                    <Text style={{ color: 'rgba(1,33,92,255)', fontSize: 9, fontWeight: '700' }}>  3:00 pm, Tomorrow</Text>
                                </View>
                                <View style={{ borderTopWidth: 1, borderRightWidth: 1, marginVertical: 5, padding: 4, borderStyle: 'dashed', borderColor: Colors.common.MAIN2, backgroundColor: 'rgba(243,242,243,255)' }}>
                                    <Text style={{ color: Colors.common.MAIN2, }}>Rs {expertDetails.visitClinic}</Text>
                                    <Text style={{ fontSize: 9, color: 'black', fontWeight: '700' }}>Visit Clinic</Text>
                                </View>
                            </View>
                        </View>

                        <TouchableOpacity onPress={() => hide(true)} style={{ alignSelf: 'center', flexDirection: 'row', alignItems: 'center', marginVertical: 50 }}>
                            <Text style={{ color: Colors.common.MAIN2, fontWeight: '500' }}>Book Appointment </Text>
                            <Image style={{ height: 15, width: 15, tintColor: Colors.common.MAIN2, }} source={{ uri: 'https://cdn2.iconfinder.com/data/icons/picons-basic-2/57/basic2-290_arrow_right-256.png' }} />
                        </TouchableOpacity>
                    </View>
                    {/* After Clicking Button Book Appointment */}
                    {
                        show ? (
                            <View style={{ backgroundColor: '#f3f2f3', borderWidth: 1, borderColor: 'rgba(231,230,234,255)', height: Dimensions.get('screen').height / 1, borderRadius: 30, paddingHorizontal: 18, }}>
                                <Text style={{ color: 'black', fontWeight: '600', marginVertical: 25, }}>Book Appointment with <Text style={{ color: Colors.common.MAIN2, }}>{expertDetails.name}</Text></Text>
                                {/* Consultaion online */}

                                <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                                    <Text style={{ flexGrow: 1, fontSize: 10, color: 'black' }}>Consult Online (View Call or Chat)</Text>
                                    <Text style={{ color: 'black', fontWeight: 'bold' }}>Rs {expertDetails.onlineConsultion}</Text>
                                </View>

                                {/* Visiting Clinic physically */}
                                <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                                    <Text style={{ flexGrow: 1, fontSize: 10, color: 'black' }}>Visit Clinic (Physically) </Text>
                                    <Text style={{ color: 'black', fontWeight: 'bold' }}>Rs {expertDetails.visitClinic}</Text>
                                </View>

                                <TouchableOpacity onPress={() => { navigation.navigate('patientDetail') }} style={{ flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center' }}>
                                    <Text style={{ color: Colors.common.MAIN2, }}>Continue </Text>
                                    <Image style={{ height: 15, width: 15, tintColor: Colors.common.MAIN2, }} source={{ uri: 'https://cdn2.iconfinder.com/data/icons/picons-basic-2/57/basic2-290_arrow_right-256.png' }} />
                                </TouchableOpacity>
                            </View>) : null}

                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 8,
        paddingVertical: 2,
        fontWeight: '500',
        color: 'black',
    }
})