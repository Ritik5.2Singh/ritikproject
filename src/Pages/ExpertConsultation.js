import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity } from 'react-native'
import React, { useEffect } from 'react'
import { Colors } from '../Colors'
import { useDispatch, useSelector } from 'react-redux'
import { searchDocDashboard } from '../REDUX/reducers/test_reducer'


export default function ExpertConsultation({ navigation }) {

    const expertsData = useSelector((state) => state.user.doctData.doctData)


    const dispatch = useDispatch()
    // for navigating to book appointment

    return (
        <ScrollView>
            <Image style={{ height: 100, width: '100%' }} source={require('../Assets/Images/Slider1/expertConsultSlider.png')} />

            <Text style={{ marginVertical: 20, marginHorizontal: 10, color: 'black', fontWeight: 'bold', fontSize: 10 }}>10 Doctors online Consultation</Text>
            <View style={{ flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'center' }}>
                {
                    expertsData.map((item) => {
                        return (
                            <TouchableOpacity key={item.id} onPress={() => { navigation.navigate('expertDetails'), dispatch(searchDocDashboard({ searchedExpert: item })) }} style={{
                                borderWidth: 1 / 2, borderColor: 'white', elevation: 2, height: 60, width: 180, flexDirection: 'row', alignItems: 'center', margin: 4, shadowColor: '#171717',
                                shadowOffset: { width: -2, height: 4 },
                                shadowOpacity: 1,
                                shadowRadius: 5,
                            }}>
                                {/* Doctor's Photo */}
                                <Image style={{ height: 40, width: 40, borderRadius: 50, borderColor: 'rgba(210,210,214,255)', marginHorizontal: 10 }} source={item.imgSrc} />
                                <View style={{ flexGrow: 1, marginHorizontal: 5, }}>
                                    {/* Doctor's Name */}
                                    <Text style={{ color: 'rgba(13,42,100,255)', fontSize: 11, fontWeight: '600', }}>{item.name}</Text>
                                    {/* Doctor's Ocupation */}
                                    <Text style={{ fontSize: 8, fontWeight: '400', color: Colors.common.MAIN2 }}>{item.work}</Text>
                                    {/* Doctor's experience */}
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 6, fontWeight: '400', }}>{item.time} </Text>
                                        <Text style={{ fontSize: 6, fontWeight: '400', }}>Experience</Text>
                                    </View>
                                    {/* Doctor's Rating */}
                                    <Text style={{ fontSize: 6, fontWeight: '400', }}>{item.rating}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }
                    )
                }

            </View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({})