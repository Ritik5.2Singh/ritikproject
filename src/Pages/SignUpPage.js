import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Dimensions, Alert } from 'react-native'
import React, { useEffect, useState } from 'react'
import { Colors } from '../Colors'

import { UserMobilefunction } from '../REDUX/Action/modify'
import { useDispatch, } from 'react-redux'
import { login } from '../REDUX/reducers/test_reducer'

function SignUpPage({ navigation }) {

    // const user = useSelector((state) => state.user)
    const dispatch = useDispatch()
    const [mobileNo, setMobileNo] = useState('');

    return (
        <>
            <View style={{ marginHorizontal: 20, marginTop: 80 }}>
                {/* Back Button */}
                <TouchableOpacity onPress={() => { navigation.goBack() }}>
                    <Image style={{ height: 20, width: 20, marginVertical: 15 }} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/essentials-pack/96/left_arrow_back_previous_navigation-512.png' }} />
                </TouchableOpacity>
                <Text style={{ color: 'black', fontSize: 20, fontWeight: '700' }}>Sign <Text style={{ color: Colors.common.MAIN2 }}>Up</Text></Text>

                <Text style={{ fontSize: 12, marginVertical: 5 }}>Please Enter Your Mobile No</Text>

                {/* Input */}
                <View style={{ borderWidth: 1 / 2, borderRadius: 5, borderColor: 'rgba(142,142,145,255)', flexDirection: 'row', marginVertical: 25, paddingVertical: 8, }}>
                    <TextInput style={{ padding: 0, marginVertical: -4, marginLeft: 6 }} placeholderTextColor={'rgba(201,197,200,255)'} editable={false} >
                        +91
                    </TextInput>

                    <View style={{ borderWidth: 1 / 2, borderColor: 'rgba(178,178,179,255)', marginHorizontal: 10 }} />

                    <TextInput style={{ padding: 0, marginVertical: -4 }} placeholderTextColor={'rgba(201,197,200,255)'} placeholder='Mobile No' onChangeText={(mobileno) => {
                        setMobileNo(mobileno)
                    }} maxLength={10}>
                    </TextInput>
                </View>

                {/* Button for Submitting mobile no */}
                <TouchableOpacity onPress={() => {
                    dispatch(login({ mobileNo: mobileNo })), navigation.navigate('otpPage')
                }} style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ color: Colors.common.MAIN2, fontSize: 16 }}>Sign up </Text>
                    <Image style={{ height: 15, width: 15, tintColor: Colors.common.MAIN2 }} source={{ uri: 'https://cdn3.iconfinder.com/data/icons/general-70/24/log-out-04-512.png' }} />
                </TouchableOpacity>
            </View>
        </>
    )
}

const styles = StyleSheet.create({})

// USING REDUX

export default SignUpPage;