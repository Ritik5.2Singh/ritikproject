import { Dimensions, FlatList, Image, ImageBackground, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { Colors } from '../Colors'


export default function SplashScreen({ navigation }) {
    const Dots = [
        {
            id: 0,
            src: require('../Assets/Images/Doctor.png'),
        },
        {
            id: 1,
            src: require('../Assets/Images/splash4.jpg'),
        },
        {
            id: 2,
            src: require('../Assets/Images/splash2.jpg'),
        },
        {
            id: 3,
            src: require('../Assets/Images/splash3.jpg'),
        },
    ]

    const Cities = [
        {
            id: 0,
            city: 'Almora',
        },
        {
            id: 1,
            city: 'Dehra Dun',
        },
        {
            id: 2,
            city: 'Haridwar',
        },
        {
            id: 3,
            city: 'Mussoorie',
        },
        {
            id: 4,
            city: 'Nainital',
        },
        {
            id: 5,
            city: 'Pithoragarh',
        },
    ]
    const [dotColor, setDotColor] = useState(0);
    //For searching Data
    const [searchData, setSearchData] = useState(false);
    const [search, setSearch] = useState('');
    const [filteredDataSource, setFilteredDataSource] = useState([]);
    const [masterDataSource, setMasterDataSource] = useState([]);
    //For Spashphotos changing

    const [splashSrc, setSplashSrc] = useState(require('../Assets/Images/Doctor.png'));
    function btn(button) {
        console.log(button, 'It is Clicked.');
        setDotColor(button.id)
        setSplashSrc(button.src)
    }

    const searchFilterFunction = (text) => {
        // Check if searched text is not blank
        if (text) {
            // Inserted text is not blank
            // Filter the masterDataSource
            // Update FilteredDataSource
            const newData = Cities.filter(
                function (item) {
                    const itemData = item.city
                        ? item.city.toUpperCase()
                        : ''.toUpperCase();
                    const textData = text.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                });
            setFilteredDataSource(newData);
            setSearch(text);
        } else {
            // Inserted text is blank
            // Update FilteredDataSource with masterDataSource
            setFilteredDataSource(masterDataSource);
            setSearch(text);
        }
    };

    return (
        <ScrollView contentContainerStyle={{ backgroundColor: Colors.light.BackG, }}>
            <View style={{ alignItems: 'center', }}>
                {/* LOGO */}
                <Image style={{ aspectRatio: 2, height: Dimensions.get('screen').height / 7, width: Dimensions.get('screen').width / 2, marginVertical: 40 }} source={require('../Assets/Images/logo.png')} />
                {/* DoctorPic */}
                <Image style={{ aspectRatio: 1, height: Dimensions.get('screen').height / 2.5, width: '100%', }} source={splashSrc} />

                <Text style={{ color: Colors.dark.TEXT, marginVertical: 22 }}>Certified Partner Doctors You can Trust</Text>

                <View style={{ flexDirection: 'row' }}>
                    {
                        Dots.map((item, index) => {
                            return (
                                <TouchableOpacity key={item.id} onPress={() => btn(item)}>
                                    <View style={{ borderWidth: 4, borderColor: index == dotColor ? Colors.common.MAIN2 : 'white', borderRadius: 50, marginHorizontal: 2 }} />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>

                {/* Straight Line */}
                <View style={{ borderWidth: 1 / 1.5, marginVertical: 30, borderColor: Colors.dark.TEXT, width: '100%' }} />
            </View>
            {/* Select City or lacality INPUT */}
            <View style={{ marginHorizontal: 20, }}>
                <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#818499', borderRadius: 5, alignItems: 'center' }}>
                    <TextInput onPressIn={() => setSearchData(true)} placeholder='Search City or Locality' placeholderTextColor={'rgba(200,196,200,255)'} style={{ flexGrow: 1, marginVertical: -8, color: 'white' }} onChangeText={(text) => searchFilterFunction(text)} />

                    <Image style={{ height: 20, width: 20, tintColor: Colors.dark.TEXT, }} source={{ uri: 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-search-256.png' }} />
                </View>
                {
                    searchData ?
                        <View style={{ backgroundColor: 'white', borderWidth: 1, borderRadius: 5, }} >
                            {
                                filteredDataSource.map((item, index) => {
                                    return (
                                        <TouchableOpacity key={item.id}>
                                            <Text style={{ color: 'gray' }}>{item.city}</Text>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </View> : null
                }
            </View>
            {/* For current Location */}
            <TouchableOpacity style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ color: Colors.common.MAIN1, fontWeight: '600', marginVertical: 10, flexGrow: 1 }}>Use Current Location</Text>
                <Image style={{ height: 20, width: 20, tintColor: Colors.common.MAIN1, }} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/e-commerce-update-1/42/target_location-256.png' }} />
            </TouchableOpacity>
            {/* For SignUp */}
            <TouchableOpacity onPress={() => { navigation.navigate('signUp') }} style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ color: Colors.common.MAIN1, fontWeight: '600', marginVertical: 10 }}>Sign up </Text>
                <Image style={{ height: 15, width: 15, tintColor: Colors.common.MAIN1, }} source={{ uri: 'https://cdn3.iconfinder.com/data/icons/remixicon-system/24/logout-circle-r-line-256.png' }} />
            </TouchableOpacity>
        </ScrollView>
    )
}

const styles = StyleSheet.create({})