import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
  Pressable,
  Animated,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {Colors} from '../Colors';
import {connect, useSelector, useDispatch} from 'react-redux';
import {DoctorInfo, searchDocDashboard} from '../REDUX/reducers/test_reducer'; //REDUX DATA
import Carousel from 'react-native-snap-carousel';
import PushNotification from 'react-native-push-notification';
function sendNotification() {
  PushNotification.localNotification({
    title: 'My Notification Title',
    message: 'My Notification Message',
  });
}
// import { selectUser } from '../REDUX/reducers/test_reducer'
//For Animation
const animatedValue = new Animated.Value(0);
function HomePageScreen({navigation}) {
  const Slider = [
    {
      id: 1,
      imgSrc: require('../Assets/Images/Slider1/one.png'),
    },
    {
      id: 2,
      imgSrc: require('../Assets/Images/Slider1/two.webp'),
    },
    {
      id: 3,
      imgSrc: require('../Assets/Images/Slider1/three.webp'),
    },
    {
      id: 4,
      imgSrc: require('../Assets/Images/Slider1/one.png'),
    },
    {
      id: 5,
      imgSrc: require('../Assets/Images/Slider1/two.webp'),
    },
    {
      id: 6,
      imgSrc: require('../Assets/Images/Slider1/three.webp'),
    },
  ];

  const Slider2 = [
    {
      id: 1,
      imgSrc: require('../Assets/Images/Slider1/one.png'),
      name: 'Covid',
    },
    {
      id: 2,
      imgSrc: require('../Assets/Images/Slider1/two.webp'),
      name: 'Physician',
    },
    {
      id: 3,
      imgSrc: require('../Assets/Images/Slider1/three.webp'),
      name: 'Gynecology',
    },
    {
      id: 4,
      imgSrc: require('../Assets/Images/Slider1/one.png'),
      name: 'Pediatrics',
    },
    {
      id: 5,
      imgSrc: require('../Assets/Images/Slider1/two.webp'),
      name: 'Dermatalogy',
    },
    {
      id: 6,
      imgSrc: require('../Assets/Images/Slider1/three.webp'),
      name: 'Psychiatry',
    },
    {
      id: 7,
      imgSrc: require('../Assets/Images/Slider1/one.png'),
      name: 'ENT',
    },
    {
      id: 8,
      imgSrc: require('../Assets/Images/Slider1/two.webp'),
      name: 'Orthopedics',
    },
    {
      id: 9,
      imgSrc: require('../Assets/Images/Slider1/three.webp'),
      name: 'Nutrition',
    },
    {
      id: 10,
      imgSrc: require('../Assets/Images/Slider1/three.webp'),
      name: 'Cardialogy',
    },
    {
      id: 11,
      imgSrc: require('../Assets/Images/Slider1/three.webp'),
      name: 'Psychiatry',
    },
  ];

  const DoctorsData = [
    {
      id: 1,
      imgSrc: require('../Assets/Images/DoctorsPic/one.webp'),
      name: 'Dr.Prashant Patel',
      work: 'Pediatrics',
      time: '10 years',
      rating: '4.4',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 196,
      onlineConsultion: 500,
      visitClinic: 700,
    },
    {
      id: 2,
      imgSrc: require('../Assets/Images/DoctorsPic/two.webp'),
      name: 'Dr.Mashoor Gulati',
      work: 'Pediatrics',
      time: '14 years',
      rating: '4.8',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 276,
      onlineConsultion: 700,
      visitClinic: 300,
    },
    {
      id: 3,
      imgSrc: require('../Assets/Images/DoctorsPic/three.webp'),
      name: 'Dr.Anup Singh',
      work: 'Pediatrics',
      time: '13 years',
      rating: '4.5',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 201,
      onlineConsultion: 700,
      visitClinic: 900,
    },
    {
      id: 4,
      imgSrc: require('../Assets/Images/DoctorsPic/four.webp'),
      name: 'Dr.Arohi Shah',
      work: 'Pediatrics',
      time: '19 years',
      rating: '4.5',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 205,
      onlineConsultion: 450,
      visitClinic: 890,
    },
    {
      id: 5,
      imgSrc: require('../Assets/Images/DoctorsPic/five.webp'),
      name: 'Dr.Vikash Joshi',
      work: 'Pediatrics',
      time: '8 years',
      rating: '4.9',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 360,
      onlineConsultion: 580,
      visitClinic: 770,
    },
    {
      id: 6,
      imgSrc: require('../Assets/Images/DoctorsPic/six.webp'),
      name: 'Dr.Vikram Joshi',
      work: 'Pediatrics',
      time: '15 years',
      rating: '4.8',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 220,
      onlineConsultion: 210,
      visitClinic: 100,
    },
    {
      id: 7,
      imgSrc: require('../Assets/Images/DoctorsPic/seven.webp'),
      name: 'Dr.Vinod Negi',
      work: 'Naturopathic',
      time: '12 years',
      rating: '4.5',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 303,
      onlineConsultion: 1000,
      visitClinic: 900,
    },
    {
      id: 8,
      imgSrc: require('../Assets/Images/DoctorsPic/eight.webp'),
      name: 'Dr.Prabhat Rawat',
      work: 'Pediatrics',
      time: '9 years',
      rating: '4.0',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 202,
      onlineConsultion: 900,
      visitClinic: 800,
    },
    {
      id: 9,
      imgSrc: require('../Assets/Images/DoctorsPic/nine.webp'),
      name: 'Dr.Hemant Joshi',
      work: 'Pediatrics',
      time: '5 years',
      rating: '4.7',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 180,
      onlineConsultion: 1100,
      visitClinic: 790,
    },
    {
      id: 10,
      imgSrc: require('../Assets/Images/DoctorsPic/ten.webp'),
      name: 'Dr.Priyadeep Chaudhari',
      work: 'Pediatrics',
      time: '11 years',
      rating: '4.1',
      bookAppointment: 'expertDetails',
      OnlineConsultantCompleted: 280,
      onlineConsultion: 680,
      visitClinic: 940,
    },
  ];

  const [search, setSearch] = useState('');
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  //Navigate to the screen
  const [searchedNavigate, setSearchedNavigate] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(DoctorInfo({doctData: DoctorsData})); //For Doctors data
  });
  // Searching DOCTORS AND HEALTH ISSUES
  const Searching = text => {
    // Check if searched text is not blank
    if (text) {
      const newData = DoctorsData.filter(item => {
        const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();

        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(DoctorsData);
      setSearch(text);
    }
  };

  const SearchedButton = item => {
    console.log('SearchedButton Clicked:- ', item);
    setSearch(item.name);
    setSearchedNavigate(item);
    dispatch(searchDocDashboard({searchedExpert: searchedNavigate}));
    Searching(item.name);
    if (search == item.name) {
      navigation.navigate(item.bookAppointment);
    }
  };
  //For First Slider using carousel
  const renderItem = ({item}) => (
    <TouchableOpacity style={styles.carouselItemContainer}>
      <Image
        source={item.imgSrc}
        style={styles.carouselItemImage}
        resizeMode="stretch"
      />
    </TouchableOpacity>
  );
  //For First Slider using Flatlist
  const [currentIndex, setCurrentIndex] = useState(0);
  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIndex((currentIndex + 1) % Slider.length);
    }, 2000);
    return () => clearInterval(interval);
  });
  return (
    <Pressable onPress={() => setFilteredDataSource([])}>
      <ScrollView>
        {/* <Image style={{ height: 100, width: '100%' }} source={require('../Assets/Images/Slider1/one.png')} /> */}
        <View style={{alignItems: 'center'}}>
          <Carousel
            data={Slider}
            renderItem={renderItem}
            sliderWidth={Dimensions.get('screen').width / 1}
            itemWidth={200}
            containerCustomStyle={{height: 120, width: '100%'}}
            autoplay={{
              delay: 1000,
              disableOnInteraction: false,
            }}
            loop={true}
          />
        </View>

        <FlatList
          keyExtractor={item => item.id}
          data={[Slider[currentIndex]]}
          horizontal
          renderItem={({item}) => (
            <TouchableOpacity style={styles.carouselItemContainer}>
              <Image
                source={item.imgSrc}
                style={{height: 150, width: Dimensions.get('screen').width / 1}}
                resizeMode="stretch"
              />
            </TouchableOpacity>
          )}
          numColumns={1}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
        />

        <View style={{margin: 10}}>
          <View
            style={{
              borderWidth: 1 / 1.5,
              borderRadius: 2,
              borderColor: 'rgba(176,176,178,255)',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            {/* SERACHING OF DOCTORS AND HEALTH ISSUES */}
            <TextInput
              value={search}
              onChangeText={doctors => {
                Searching(doctors);
              }}
              style={{flexGrow: 1, color: 'blue'}}
              placeholder="Search a Doctor or Health issues"
              placeholderTextColor={'rgba(201,197,200,255)'}
            />
            <Image
              style={{
                height: 20,
                width: 20,
                marginRight: 22,
                tintColor: 'rgba(201,197,200,255)',
              }}
              source={{
                uri: 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-search-strong-256.png',
              }}
            />
          </View>

          {filteredDataSource.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  SearchedButton(item);
                }}
                style={{backgroundColor: '#E6EBEC', borderBottomWidth: 1}}>
                <Text style={{paddingVertical: 10, color: 'brown'}}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>

        <View style={{marginHorizontal: 5}}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {Slider.map((item, index) => {
              return (
                <TouchableOpacity key={index}>
                  <Image
                    style={{
                      marginHorizontal: 5,
                      height: 70,
                      width: 100,
                      resizeMode: 'stretch',
                    }}
                    source={item.imgSrc}
                  />
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
        <Text
          style={{
            marginVertical: 30,
            marginHorizontal: 15,
            color: 'black',
            fontWeight: 'bold',
          }}>
          What are your Symptoms?
        </Text>

        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {Slider2.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                style={{alignItems: 'center', marginHorizontal: 9}}
                onPress={() => {
                  navigation.navigate('doctorCategory');
                }}>
                <View
                  style={{
                    borderWidth: 2,
                    borderColor: 'black',
                    borderRadius: 50,
                    borderColor: Colors.common.MAIN2,
                    borderStyle: 'dashed',
                  }}>
                  <Image
                    style={{
                      borderRadius: 50,
                      height: 75,
                      width: 75,
                      borderWidth: 2,
                    }}
                    source={item.imgSrc}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 9,
                    color: 'black',
                    fontWeight: '800',
                    marginVertical: 10,
                  }}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>

        <TouchableOpacity
          onPress={sendNotification}
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 15,
          }}>
          <Text style={{color: Colors.common.MAIN2, fontWeight: '500'}}>
            View All Symptoms{' '}
          </Text>
          <Image
            style={{height: 15, width: 15, tintColor: Colors.common.MAIN2}}
            source={{
              uri: 'https://cdn2.iconfinder.com/data/icons/picons-basic-2/57/basic2-290_arrow_right-256.png',
            }}
          />
        </TouchableOpacity>
      </ScrollView>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  carouselContainer: {
    height: 200,
  },
  carouselItemContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 3,
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#333',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  carouselItemImage: {
    width: '100%',
    height: 200,
    borderRadius: 10,
  },
  carouselItemTitle: {
    color: '#000',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default HomePageScreen;
