import { StyleSheet, Text, TextInput, View, TouchableOpacity, Image, Dimensions } from 'react-native'
import React, { useState } from 'react'
import { Colors } from '../Colors'
import { useDispatch, useSelector } from 'react-redux'
import { ScheduleAppointment } from '../REDUX/reducers/test_reducer'

export default function PatientDetails({ navigation }) {
    //Appointment going to Booked
    const DoctorTobeBooked = useSelector((state) => state.user.searchedExpert.searchedExpert)
    console.log('Doctor going to Booked :-' + JSON.stringify(DoctorTobeBooked, null, 1));

    const [input, setInput] = useState([]);

    const dispatchPatientDetails = useDispatch();

    const InputData = [
        {
            key: 0,
            placeholder: 'Full Name',
            value: '',
        },
        {
            key: 1,
            placeholder: 'Age',
            value: '',
        },
        {
            key: 2,
            placeholder: 'Address',
            value: '',
        },
        {
            key: 3,
            placeholder: 'Email',
            value: '',
        },
        {
            key: 4,
            placeholder: 'Mobile Number',
            value: '',
        },
        {
            key: 5,
            placeholder: 'Any descrition about Diagnosis',
            value: '',
        },
    ]
    const handleInput = (value, inputText) => {
        setInput({ ...input, [value.placeholder]: inputText });
        console.log('INPUT:-' + JSON.stringify(input, null, 1));
    }

    const onSubmit = () => {

        if (input != null) {
            navigation.navigate('schedualTimePage')
            //Input DATA
            // setInput({ ...input, [value]: inputText });
            // console.log('INPUT:-' + JSON.stringify(input, null, 1));
            // const patient = JSON.stringify(input, null, 1);
            console.log('DATA of Patient=>' + input);

            // let appointment = Object.assign({}, input, DoctorTobeBooked, newObj);
            // console.log('DATA of APPOINTMENT=>' + JSON.stringify(appointment, null, 1));
            // dispatchPatientDetails(ScheduleAppointment({ scheduleAppointment: appointment }));
        }
        else {
            alert('Please fill all fields!')
        }
    }
    return (
        <View>
            {
                InputData.map((item, index) => {
                    return (
                        <View key={item.key} style={{ borderWidth: 1, borderColor: 'rgba(213,211,215,255)', marginHorizontal: 20, marginVertical: 6, borderRadius: 5 }}>
                            <TextInput onChangeText={text => handleInput(item, text)} placeholder={item.placeholder} maxLength={index == 4 ? 10 : 100 && index == 1 ? 3 : 100} placeholderTextColor={'rgba(213,211,215,255)'} style={{ marginHorizontal: 10, marginVertical: -9 }} />
                        </View>
                    )
                })
            }
            <TouchableOpacity onPress={() => { onSubmit() }} style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', height: Dimensions.get('screen').height / 7 }}>
                <Text style={{ color: Colors.common.MAIN2, }}>Submit </Text>
                <Image style={{ height: 15, width: 15, tintColor: Colors.common.MAIN2, }} source={{ uri: 'https://cdn2.iconfinder.com/data/icons/picons-basic-2/57/basic2-290_arrow_right-256.png' }} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({})