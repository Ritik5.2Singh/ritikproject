const SUN_FLOWER = '#f1c40f';
const ASBESTOS = '#7f8c8d';
const BackGroundColor = 'rgba(0,8,50,255)';
const EMERALD = '#2ecc71';
const MAROON = '#e74c3c';
const MAROON1 = '#a10d3f';
const MAROON2 = 'rgba(246,15,70,255)';
const TextCOLOR = '#fefeff';
const SILVER = '#bdc3c7';

const common = {
    PRIMARY: SUN_FLOWER,
    SUCCESS: EMERALD,
    MAIN: MAROON,
    MAIN1: MAROON1,
    MAIN2: MAROON2,
};

const light = {

    BACKGROUND: TextCOLOR,
    BackG: BackGroundColor,
    TEXT_SECONDARY: ASBESTOS,
};

const dark = {

    BACKGROUND: BackGroundColor,
    TEXT: TextCOLOR,
    TEXT_SECONDARY: SILVER,
};

export const Colors = { light, dark, common };