import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  Button,
  TextInput,
  Dimensions,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

import {DrawerActions} from '@react-navigation/native';
import axios from 'axios';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

export default function Maps({navigation}) {
  const [currentRegion, setCurrentRegion] = useState(null);

  const openDrawer = drawerName => {
    navigation.dispatch(DrawerActions.openDrawer('drawer'));
  };
  // For searching
  const [searchQuery, setSearchQuery] = useState('');
  const [location, setLocation] = useState(null);

  // Map Type
  const MAPTYPE = [
    {id: 0, typeName: 'Satellite', type: 'satellite'},
    {id: 1, typeName: 'Standard', type: 'standard'},
    {id: 2, typeName: 'Terrain', type: 'terrain'},
  ];
  const [mapType, setMapType] = useState('standard');
  const mapTypeFunction = mapType => {
    setMapType(mapType.type);
  };
  const handleSearch = async () => {
    try {
      const response = await axios.get(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${searchQuery}&key=YOUR_API_KEY`,
      );

      const {results} = response.data;
      if (results && results.length > 0) {
        const {geometry} = results[0];
        const {location} = geometry;
        setLocation(location);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };
  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        console.log(
          'Here is a Current Location:- ',
          JSON.stringify(position, null, 2),
        );
        const {latitude, longitude} = position.coords;
        setCurrentRegion({
          latitude,
          longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        });
      },
      error => console.log(error),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  }, []);

  return (
    <View style={styles.container}>
      {currentRegion && (
        <MapView
          style={styles.map}
          mapType={mapType}
          initialRegion={{
            latitude: currentRegion.latitude,
            longitude: currentRegion.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}>
          <Marker coordinate={currentRegion} />
          <View
            style={{
              position: 'absolute',
              width: '90%',
              backgroundColor: 'white',
              elevation: 4,
              padding: 8,
              borderRadius: 8,
              top: 40,
            }}>
            <GooglePlacesAutocomplete
              styles={{borderColor: 'grey', borderWidth: 1}}
              placeholder="Search"
              onPress={(data, details) => {
                console.log('Data:-', data + ' ||| Data Details:-', details);
              }}
            />
          </View>
        </MapView>
      )}
      {/* Search button */}
      <View
        style={{
          backgroundColor: 'white',
          width: '95%',
          margin: 5,
          borderRadius: 50,
          flexDirection: 'row',
          alignItems: 'center',
          borderWidth: 1,
          borderColor: 'grey',
        }}>
        {/* Drawer Button */}
        <TouchableOpacity onPress={() => openDrawer('drawer')}>
          <Image
            style={{height: 15, width: 15, marginLeft: 8}}
            source={{
              uri: 'https://cdn1.iconfinder.com/data/icons/free-ui-1/24/hamburger-menu-ui-ux-mobile-web-256.png',
            }}
          />
        </TouchableOpacity>
        {/* Search Here */}
        <TextInput
          placeholder="Search here"
          placeholderTextColor={'grey'}
          style={{flex: 1, paddingVertical: 5}}
        />
        <Image
          style={{height: 20, width: 20, marginLeft: 8, marginRight: 10}}
          source={{
            uri: 'https://cdn3.iconfinder.com/data/icons/line-icons-set/128/1-03-512.png',
          }}
        />
      </View>
      {/* Map types */}
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        {MAPTYPE.map((item, index) => (
          <TouchableOpacity
            onPress={() => {
              mapTypeFunction(item);
            }}
            key={index}
            style={{flex: 1, alignItems: 'center'}}>
            <Text
              style={{
                color: '#006F6E',
                fontSize: 12,
                fontWeight: 'bold',
                backgroundColor: 'white',
                borderRadius: 6,
                padding: 5,
                elevation: 2,
                borderWidth: 1,
              }}>
              {item.typeName}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    // height: Dimensions.get('screen').height / 1,
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height: Dimensions.get('screen').height / 1,
  },
});
