import { StyleSheet, Text, View } from 'react-native'
import React from 'react'


import { createDrawerNavigator } from '@react-navigation/drawer';
import Maps from './Maps';
import Settings from './Settings';


const Drawer = createDrawerNavigator();
export default function MapDrawer() {
    return (

        <Drawer.Navigator drawerName='drawer'>
            <Drawer.Screen name='maps' component={Maps} options={{ headerShown: false }} />
            <Drawer.Screen name='mapSettings' component={Settings} options={{ headerShown: false }} />
        </Drawer.Navigator>

    )
}

const styles = StyleSheet.create({})