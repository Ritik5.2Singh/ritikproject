import React, {useState} from 'react';
import {View, Text, TextInput, Button} from 'react-native';
import RNHTMLtoPDF from 'react-native-html-to-pdf';

const DownloadingPdf = () => {
  const [input, setInput] = useState('');
  const [pdf, setPdf] = useState(null);

  const createPDF = async () => {
    const options = {
      html: `<h1>${input}</h1>`,
      fileName: 'helloPDF',
      directory: 'docs',
    };

    const file = await RNHTMLtoPDF.convert(options);
    setPdf(file.filePath);
  };

  return (
    <View>
      <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={text => setInput(text)}
        value={input}
      />
      <Button title="Create PDF" onPress={createPDF} />
      {pdf && <Text>PDF generated at: {pdf}</Text>}
    </View>
  );
};

export default DownloadingPdf;
