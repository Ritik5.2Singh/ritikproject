import { Alert, Button, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import messaging from '@react-native-firebase/messaging';
import NotificationController from '../../NotificationController';

export default function PushNotification() {

    useEffect(() => {
        messaging().setBackgroundMessageHandler(async remoteMessage => {

        })

        const unsubscribe = messaging().onMessage(async remoteMessage => {
            console.log('The Notification Message:- ' + JSON.stringify(remoteMessage, null, 1))
        })
        return unsubscribe;
    }, [])

    const checkToken = async () => {
        const fcmToken = await messaging().getToken();
        if (fcmToken) {
            console.log(fcmToken);
            Alert.alert(JSON.stringify(fcmToken));
        }
    }

    return (
        <View>
            <NotificationController />
            <Text>PUSH NOTIFICATION with FIREBASE Demo</Text>
            <Button title='Get FCM Token' onPress={() => { checkToken() }} />
        </View>
    )
}

const styles = StyleSheet.create({})